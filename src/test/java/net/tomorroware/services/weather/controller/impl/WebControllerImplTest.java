package net.tomorroware.services.weather.controller.impl;

import net.tomorroware.services.weather.integration.WeatherService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * Created by Gabriel on 06/07/2016.
 */
public class WebControllerImplTest {

    @InjectMocks
    private WebControllerImpl webController;

    @Mock
    private WeatherService weatherService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetHomeView() throws Exception {
        assertNotNull(webController);
        assertNotNull(weatherService);
        String viewId = webController.getHomeView();
        assertEquals("index", viewId);
        verifyZeroInteractions(weatherService);
    }

    @Test
    public void testGetWeather() throws Exception {
        assertNotNull(webController);
        assertNotNull(weatherService);
        Model mockModel = mock(Model.class);
        String cityId = "[valid_key]";
        Map<String, Object> weatherInfo = new HashMap<>();
        weatherInfo.put("[valid_weather_attribute]", "[valid_weather_value]");
        when(weatherService.getWeatherInfo(cityId)).thenReturn(weatherInfo);

        String viewId = webController.getWeather(cityId, mockModel);
        assertEquals("results", viewId);
        verify(weatherService, times(1)).getWeatherInfo(cityId);
        verify(mockModel, times(1)).addAllAttributes(anyMapOf(String.class, Object.class));
    }

    @Test
    public void testGetWeatherWithNullCityId() {
        assertNotNull(webController);
        assertNotNull(weatherService);
        Model mockModel = mock(Model.class);
        String cityId = null;
        String viewId = webController.getWeather(cityId, mockModel);
        assertEquals("redirect:/pages/no_city.htm", viewId);
        verifyZeroInteractions(weatherService);
    }

    @Test
    public void testGetWeatherWithEmptyStringCityId() {
        assertNotNull(webController);
        assertNotNull(weatherService);
        Model mockModel = mock(Model.class);
        String cityId = "";
        String viewId = webController.getWeather(cityId, mockModel);
        assertEquals("redirect:/pages/no_city.htm", viewId);
        verifyZeroInteractions(weatherService);
    }

    @Test
    public void testGetWeatherWithBadServiceResponses() {
        assertNotNull(webController);
        assertNotNull(weatherService);
        Model mockModel = mock(Model.class);
        String cityId = "[valid_key]";

        // Test for 'null' service response
        String viewId = webController.getWeather(cityId, mockModel);
        assertEquals("redirect:/pages/no_results.htm", viewId);

        // Test for empty map service response
        when(weatherService.getWeatherInfo(cityId)).thenReturn(Collections.emptyMap());
        viewId = webController.getWeather(cityId, mockModel);
        assertEquals("redirect:/pages/no_results.htm", viewId);
        verify(weatherService, times(2)).getWeatherInfo(cityId);
    }
}