package net.tomorroware.services.weather.integration.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.tomorroware.services.weather.integration.WeatherService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * Created by Gabriel on 06/07/2016.
 */
public class WeatherServiceImplTest {
    private static final Logger LOG = LoggerFactory.getLogger(WeatherServiceImplTest.class);
    private static final String VALID_RESPONSE;
    private static final String INVALID_RESPONSE;

    static {
        VALID_RESPONSE = "{\"city\":{\"id\":2328926,\"name\":\"Federal Republic of Nigeria\",\"coord\":{\"lon\":8,\"lat\":10},\"country\":\"NG\",\"population\":0,\"sys\":{\"population\":0}},\"cod\":\"202\",\"message\":0.0025," +
                "\"list\":[{\"dt\":1467849600,\"main\":{\"temp\":294.07,\"temp_min\":294.07,\"temp_max\":294.454,\"pressure\":942.7,\"sea_level\":1026.96,\"grnd_level\":942.7,\"humidity\":97,\"temp_kf\":-0.38},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":76},\"wind\":{\"speed\":3.17,\"deg\":204.501},\"rain\":{},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2016-07-07 00:00:00\"}]}";
        INVALID_RESPONSE = "{\"cod\":\"404\",\"message\":\"Error: Not found city\"}";
    }

    @InjectMocks
    private WeatherServiceImpl weatherService;

    @Mock
    private RestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetWeatherInfo() throws IOException {
        assertNotNull(weatherService);
        assertNotNull(restTemplate);
        ResponseEntity<LinkedHashMap> response = getResponse(VALID_RESPONSE);
        when(restTemplate.getForEntity(any(URI.class), eq(LinkedHashMap.class))).thenReturn(response);

        String cityId = "[valid_key]";
        Map<String, Object> weatherInfo = weatherService.getWeatherInfo(cityId);
        assertEquals("202", response.getBody().get("cod"));
        assertNotNull(weatherInfo);
        assertFalse(weatherInfo.isEmpty());
        assertEquals("Federal Republic of Nigeria", weatherInfo.get("city"));
        assertEquals("Clouds", weatherInfo.get("weatherGeneral"));
        assertEquals("broken clouds", weatherInfo.get("weatherDescription"));
        Double temperatureF = (Double) weatherInfo.get("temperatureF");
        assertEquals(294.07, temperatureF, 0);
        assertEquals(WeatherService.convertTemperatureFToC(temperatureF), weatherInfo.get("temperatureC"));
        assertTrue(weatherInfo.containsKey("sunrise"));
        assertTrue(weatherInfo.containsKey("sunset"));
        assertNull(weatherInfo.get("sunrise"));
        assertNull(weatherInfo.get("sunset"));
        LOG.info("Returned info:  {}", weatherInfo);
    }

    @Test
    public void testNullCityId() {
        String cityId = null;
        Map<String, Object> weatherInfo = weatherService.getWeatherInfo(cityId);
        assertNotNull(weatherInfo);
        assertTrue(weatherInfo.isEmpty());
        verifyZeroInteractions(restTemplate);
    }

    @Test
    public void testEmptyStringCityId() {
        String cityId = "";
        Map<String, Object> weatherInfo = weatherService.getWeatherInfo(cityId);
        assertNotNull(weatherInfo);
        assertTrue(weatherInfo.isEmpty());
        verifyZeroInteractions(restTemplate);
    }

    @Test
    public void testUnrecognisedCityId() throws IOException {
        assertNotNull(weatherService);
        assertNotNull(restTemplate);
        ResponseEntity<LinkedHashMap> response = getResponse(INVALID_RESPONSE);
        when(restTemplate.getForEntity(any(URI.class), eq(LinkedHashMap.class))).thenReturn(response);

        String cityId = "[invalid_key]";
        Map<String, Object> weatherInfo = weatherService.getWeatherInfo(cityId);
        assertNotNull(weatherInfo);
        assertTrue(weatherInfo.isEmpty());
        assertEquals("404", response.getBody().get("cod"));
    }

    @Test
    public void testRepeatRequest() throws IOException {
        assertNotNull(weatherService);
        assertNotNull(restTemplate);
        ResponseEntity<LinkedHashMap> response = getResponse(VALID_RESPONSE);
        when(restTemplate.getForEntity(any(URI.class), eq(LinkedHashMap.class))).thenReturn(response);

        String cityId = "[valid_key]";
        Map<String, Object> weatherInfo = weatherService.getWeatherInfo(cityId);
        assertNotNull(weatherInfo);
        assertFalse(weatherInfo.isEmpty());
        assertEquals("Federal Republic of Nigeria", weatherInfo.get("city"));
        assertEquals("Clouds", weatherInfo.get("weatherGeneral"));
        assertEquals("broken clouds", weatherInfo.get("weatherDescription"));
        Double temperatureF = (Double) weatherInfo.get("temperatureF");
        assertEquals(294.07, temperatureF, 0);
        assertEquals(WeatherService.convertTemperatureFToC(temperatureF), weatherInfo.get("temperatureC"));
        assertTrue(weatherInfo.containsKey("sunrise"));
        assertTrue(weatherInfo.containsKey("sunset"));
        assertNull(weatherInfo.get("sunrise"));
        assertNull(weatherInfo.get("sunset"));
        LOG.info("Returned info first time:  {}", weatherInfo);

        Map<String, Object> weatherInfo2 = weatherService.getWeatherInfo(cityId);
        assertSame(weatherInfo, weatherInfo2);
        LOG.info("Returned info:  {}", weatherInfo);
        verify(response, times(1)).getBody();
        verify(restTemplate, times(1)).getForEntity(any(URI.class), eq(LinkedHashMap.class));
    }

    @Test
    public void testNoApiKeysAvailable() throws IOException {
        assertNotNull(weatherService);
        assertNotNull(restTemplate);
        ResponseEntity<LinkedHashMap> response = getResponse(VALID_RESPONSE);
        when(restTemplate.getForEntity(any(URI.class), eq(LinkedHashMap.class))).thenReturn(response);

        String cityId = "[valid_key]";
        int count = 0;
        Map<String, Object> weatherInfo = weatherService.getWeatherInfo(cityId);
        while (!weatherInfo.isEmpty()) {
            count++;
            weatherInfo = weatherService.getWeatherInfo(cityId + count);
        }
        LOG.info("The number of valid distinct cities we were able to test tells us that we have {} keys", count);
        assertTrue(1 <= count);

        verify(response, times(count)).getBody();
        verify(restTemplate, times(count)).getForEntity(any(URI.class), eq(LinkedHashMap.class));
    }

    private ResponseEntity<LinkedHashMap> getResponse(String jsonBody) throws IOException {
        ResponseEntity<LinkedHashMap> mockResponseEntity = mock(ResponseEntity.class);
        LinkedHashMap<String, Object> responseBody = objectMapper.readValue(jsonBody, LinkedHashMap.class);
        when(mockResponseEntity.getBody()).thenReturn(responseBody);
        return mockResponseEntity;
    }
}