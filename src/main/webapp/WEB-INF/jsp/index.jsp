<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
    <head>
        <title>Weather Forecast Service</title>
    </head>

    <body>
        <h2>Weather Forecast Service</h2>
        <form:form method="GET" action="/weather">
            <p>Select a city and click 'Get Weather'</p>
            <select name="cityid">
                <option value="" selected>[Choose City]</option>
                <option value="2643743">London</option>
                <option value="1819729">Hong Kong</option>
            </select>
            <table>
                <tr>
                    <td>
                        <input type="submit" value="Get Weather"/>
                    </td>
                </tr>
            </table>
        </form:form>
    </body>
</html>