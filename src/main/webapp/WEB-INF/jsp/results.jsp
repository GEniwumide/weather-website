<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
    <head>
        <title>${city} Weather</title>
    </head>

    <body>
        <h2>Today in ${city}</h2>
        <p>As of ${date}</p>
        <p>Weather forecast:  ${weatherDescription}  (${weatherGeneral})</p>
        <p>Temperature is ${temperatureF}F  (${temperatureC}C)</p>
        <p>Time of sunrise is [not available]</p>
        <p>Time of sunset is [not available]</p>
        <form method="GET" action="/">
            <table>
                <tr>
                    <td>
                        <input type="submit" value="<  Go Back"/>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>