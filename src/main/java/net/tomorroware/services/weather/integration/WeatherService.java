package net.tomorroware.services.weather.integration;

import java.util.Map;

/**
 * Created by Gabriel on 05/07/2016.
 * <p>
 * This service will be used to query weather information.  The source of the information
 * and implementations of the service are left to the concrete class(es).
 */
public interface WeatherService {

    /**
     * Converts temperatures in degrees fahrenheit to celsius
     *
     * @param temperature the temperature in degrees fahrenheit
     * @return the equivalent temperature in degrees celsius (rounded to 2 decimal places)
     */
    static double convertTemperatureFToC(Double temperature) {
        return Math.round((temperature - 32.0) / 0.018) / 100.0;
    }

    /**
     * Retrieves a key-value map containing all weather information about
     * a given city.
     *
     * @param cityId The city identifier as issued by openweathermap.org
     * @return mapping of city and weather attributes
     * @see http://bulk.openweathermap.org/sample/city.list.json.gz
     */
    Map<String, Object> getWeatherInfo(String cityId);
}