package net.tomorroware.services.weather.integration.impl;

import net.tomorroware.services.weather.integration.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

/**
 * Created by Gabriel on 05/07/2016.
 * <p>
 * Default implementation of the Weather Service adapter
 */
@Component
public class WeatherServiceImpl implements WeatherService {
    private static final Logger LOG = LoggerFactory.getLogger(WeatherServiceImpl.class);
    private static final String WEBSERVICE_URL = "http://api.openweathermap.org/data/2.5/forecast/city";
    /**
     * Time to live (TTL) in minutes for the cached requests.
     */
    private static final int CACHE_TTL = 10;
    /**
     * Recycle time for API keys.
     */
    private static final int KEY_TTL = 10;
    /**
     * The site recommends not making calls with the same key within 10 mins.
     * To mitigate this, we can use a collection of keys.
     * TODO: error handling when a key is not available
     */
    private static final Set<String> API_KEYS;

    static {
        API_KEYS = new HashSet<>();
        API_KEYS.add("75cf129178cbe9060f29f0c61e75dfbe");
        API_KEYS.add("2595a30130347672615dc35f20e7e25d");
    }

    private Map<String, Map.Entry<Calendar, Map<String, Object>>> LAST_REQUESTS = new HashMap<>();
    private Map<String, Calendar> apiKeyTimestamps;
    private RestTemplate restTemplate;

    public WeatherServiceImpl() {
        apiKeyTimestamps = new HashMap<>();
        restTemplate = new RestTemplate();
        API_KEYS.forEach(apiKey -> apiKeyTimestamps.put(apiKey, null));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Object> getWeatherInfo(String cityId) {
        if (StringUtils.isEmpty(cityId)) {
            return Collections.emptyMap();
        } else if (LAST_REQUESTS.containsKey(cityId)) {
            Map.Entry<Calendar, Map<String, Object>> cachedRequest = LAST_REQUESTS.get(cityId);
            Calendar tenMinuesAgo = Calendar.getInstance();
            tenMinuesAgo.add(Calendar.MINUTE, -CACHE_TTL);
            // if the request for this city was made less that 10 minutes ago, we can supply the same response.
            if (cachedRequest.getKey().after(tenMinuesAgo)) {
                return cachedRequest.getValue();
            }
        }

        String apiKey = getUsableApiKey();
        if (apiKey == null) {
            LOG.warn("No valid api key was available");
            return Collections.emptyMap();
        }

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(WEBSERVICE_URL)
                .queryParam("id", cityId).queryParam("APPID", apiKey);
        ResponseEntity<LinkedHashMap> response = restTemplate.getForEntity(uriBuilder.build().encode().toUri(), LinkedHashMap.class);
        LinkedHashMap<String, Object> weatherInfo = response.getBody();

        if (weatherInfo != null && weatherInfo.containsKey("cod") && weatherInfo.get("cod").toString().startsWith("2")) {
            Map<String, Object> requiredInfo = new HashMap<>();
            Calendar today = Calendar.getInstance();
            requiredInfo.put("date", today.getTime());
            requiredInfo.put("city", ((Map) weatherInfo.get("city")).get("name"));
            List forecasts = (List) weatherInfo.get("list");
            Map todaysForecast = (Map) forecasts.get(0);
            Map weatherDetails = (Map) ((List) todaysForecast.get("weather")).get(0);
            requiredInfo.put("weatherGeneral", weatherDetails.get("main"));
            requiredInfo.put("weatherDescription", weatherDetails.get("description"));
            Double temperature = (Double) ((Map) todaysForecast.get("main")).get("temp");
            requiredInfo.put("temperatureF", temperature);
            requiredInfo.put("temperatureC", WeatherService.convertTemperatureFToC(temperature));

            // TODO:  sunrise and sunset times do not seem to be currently available in the information.
            requiredInfo.put("sunrise", null);
            requiredInfo.put("sunset", null);
            record(cityId, requiredInfo, apiKey);
            return requiredInfo;
        }

        return Collections.emptyMap();
    }

    /**
     * Records the time stamps of the last used API key and caches the
     * city the call was made for recycling.
     *
     * @param cityId       The city identifier of the last request
     * @param requiredInfo The results of the last request
     * @param apiKey       The API key used in the last request
     */
    private void record(String cityId, Map<String, Object> requiredInfo, String apiKey) {
        Calendar timestamp = Calendar.getInstance();
        apiKeyTimestamps.put(apiKey, timestamp);
        LAST_REQUESTS.put(cityId, new AbstractMap.SimpleImmutableEntry<>(timestamp, requiredInfo));
    }

    private String getUsableApiKey() {
        Calendar time = Calendar.getInstance();
        time.add(Calendar.MINUTE, -KEY_TTL);
        for (Map.Entry<String, Calendar> apiKey : apiKeyTimestamps.entrySet()) {
            if (apiKey.getValue() == null || apiKey.getValue().before(time)) {
                return apiKey.getKey();
            }
        }

        return null;
    }
}