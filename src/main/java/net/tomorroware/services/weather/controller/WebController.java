package net.tomorroware.services.weather.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Gabriel on 05/07/2016.
 * <p>
 * Application Controller interface.
 */
public interface WebController {
    /**
     * Handler for the getHomeView page request.
     *
     * @return the resolvable identifier for the landing page view
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    String getHomeView();

    /**
     * Handler for the weather request.
     *
     * @param cityId identifier of the requested city
     * @param model  used to store domain data for the resulting view to access
     * @return the resolvable identifier for the results view or redirect information
     */
    @RequestMapping(value = "/weather", method = RequestMethod.GET)
    String getWeather(@RequestParam(value = "cityid", required = true) String cityId, Model model);
}