package net.tomorroware.services.weather.controller.impl;

import net.tomorroware.services.weather.controller.WebController;
import net.tomorroware.services.weather.integration.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Created by Gabriel on 05/07/2016.
 * <p>
 * Default implementation of the Web Controller
 */
@Controller
public class WebControllerImpl implements WebController {

    @Autowired
    private WeatherService weatherService;

    /**
     * {@inheritDoc}
     */
    @Override
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getHomeView() {
        return "index";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @RequestMapping(value = "/weather", method = RequestMethod.GET)
    public String getWeather(@RequestParam(value = "cityid", required = true) String cityId, Model model) {
        // If the provided cityId is 'null' or empty string, show corresponding error page
        if (StringUtils.isEmpty(cityId)) {
            return "redirect:/pages/no_city.htm";
        }

        // If no information could be gathered from service, show corresponding error page
        Map<String, Object> weatherInfo = weatherService.getWeatherInfo(cityId);
        if (weatherInfo == null || weatherInfo.isEmpty()) {
            return "redirect:/pages/no_results.htm";
        }

        model.addAllAttributes(weatherInfo);
        return "results";
    }
}