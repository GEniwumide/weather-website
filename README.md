Weather Website
===================
This project is built with Java 8 and Maven 3.3.1

##  To build entire project (including deployable war artifact)

1)  On command line, go to project root directory (where this file sits)

2)  Run command
        mvn clean install

3)  Once built, the deployable war artifact can be located at:
        .\target\WeatherSite.war


##  To run Jetty Server

1)  On command line, go to project root directory (where this file sits)

2)  Run command to start the Jetty Server
        mvn jetty:run


##  How to make queries
Once the artifact is deployed or the jetty server is running, direct your browser to the root context url
of the web server.  For jetty, the default is http://localhost:8080

The following http requests are also available

1)  To access the home view (index.jsp)
        GET /

2)  To retrieve weather information for a given city identifier
        GET /weather?cityid=[cityId]


##  TODOs
The following items of work are still pending

1)  Pull information for sunset and sunrise times for the city  (this does not seem to be available anymore) and format of date/time to 12-hours

2)  Error handling for service over-use due to high volume of requests made to api.openweathermap.org

3)  Incorporation of other cities? (maybe ?)